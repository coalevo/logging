/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.logging.impl;

import ch.qos.logback.core.util.StatusPrinter;
import net.coalevo.logging.service.LoggingService;
import org.osgi.framework.*;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;
import org.slf4j.*;

import java.io.File;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.ResourceBundle;

/**
 * This class implements the {@link LoggingService}.
 * <p/>
 * Note that it may be obtained and used as a standard <tt>org.osgi.service.log.LogService</tt> as
 * well.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class LoggingServiceImpl
    implements ManagedService, LoggingService {

  ResourceBundle m_Resources;
  IMarkerFactory m_MarkerFactory;

  private Marker m_LogMarker;
  private Logger m_RootLogger;
  private Marker m_WrongCategory;

  //This will be called by a ConfigurationAdmin thread !!
  public void updated(Dictionary conf)
      throws ConfigurationException {

    if (conf == null) {
      //Ignore
      return;
    }

    Activator.log.debug(m_LogMarker, "updated(Dictionary)" + conf.toString());

    try {
      Object o = conf.get("configfile");
      if (o != null) {
        String str = o.toString();
        if (str.length() == 0) {
          str = System.getProperty("user.dir") + File.separator + "configuration" + File.separator + "logback.xml";
        }
        Activator.prepareLogback(str);
      }
    } catch (Exception je) {
      Activator.log.debug(m_LogMarker, "updated()", je);
    } finally {
      StatusPrinter.print(Activator.getLoggerContext());
    }
  }//update

  public boolean activate(BundleContext bc) {

    m_Resources = ResourceBundle.getBundle("OSGI-INF.l10n.strings");
    m_LogMarker = MarkerFactory.getMarker(LoggingServiceImpl.class.getName());
    m_RootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    m_WrongCategory = MarkerFactory.getMarker("[CATEGORY-WRONG]");
    m_MarkerFactory = MarkerFactory.getIMarkerFactory();

    //4. Register service
    final Hashtable<String, String> p = new Hashtable<String, String>();
    p.put(org.osgi.framework.Constants.SERVICE_PID, LoggingService.class.getName());
    String[] classes = new String[]{LoggingService.class.getName(), ManagedService.class.getName(), LogService.class.getName()};
    bc.registerService(classes,
        this,
        p);

    /*
    //5. Make sure that some config is available for editing
    ConfigurationAdmin ca = Activator.getServices().getConfigurationAdmin(ServiceMediator.WAIT_UNLIMITED);
    if (ca == null) {
      Activator.log.error(m_LogMarker, "activate():ConfigurationAdmin NULL");
      return false;
    }
    try {
      Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + LoggingService.class.getName() + ")");
      if (c == null || c.length == 0) {
        Configuration config = ca.getConfiguration(LoggingService.class.getName());

        //prepare defaults
        Hashtable<String, String> d = new Hashtable<String, String>();
        d.put("configfile", System.getProperty("user.dir") + File.separator + "configuration" + File.separator + "logback.xml");
        //update with defaults
        config.update(d);
        Activator.log.info(m_LogMarker, "config.initialized");
      }
    } catch (Exception ex) {
      Activator.log.error(m_LogMarker, "activate()", ex);
      return false;
    }
    */
    //ServiceMediator services = Activator.getServices();
    //add listeners
    bc.addFrameworkListener(new FrameworkListenerAdapter());
    bc.addBundleListener(new BundleListenerAdapter());
    bc.addServiceListener(new ServiceListenerAdapter());

    return true;
  }//activate

  public boolean deactivate() {
    m_Resources = null;
    m_LogMarker = null;
    m_RootLogger = null;
    m_WrongCategory = null;
    m_MarkerFactory = null;
    return true;
  }//deactivate

  public void log(int i, String s) {

    switch (i) {
      case LogService.LOG_DEBUG:
        m_RootLogger.debug(s);
        break;
      case LogService.LOG_ERROR:
        m_RootLogger.error(s);
        break;
      case LogService.LOG_INFO:
        m_RootLogger.info(s);
        break;
      case LogService.LOG_WARNING:
        m_RootLogger.warn(s);
        break;
      default:
        m_RootLogger.error(m_WrongCategory, s);
    }

  }//log

  public void log(int i, String s, Throwable throwable) {

    switch (i) {
      case LogService.LOG_DEBUG:
        m_RootLogger.debug(s, throwable);
        break;
      case LogService.LOG_ERROR:
        m_RootLogger.error(s, throwable);
        break;
      case LogService.LOG_INFO:
        m_RootLogger.info(s, throwable);
        break;
      case LogService.LOG_WARNING:
        m_RootLogger.warn(s, throwable);
        break;
      default:
        m_RootLogger.error(m_WrongCategory, s, throwable);
    }

  }//log

  public void log(ServiceReference serviceReference, int i, String s) {

    if (serviceReference == null) {
      log(i, s);
      return;
    }
    Marker m = MarkerFactory.getMarker(serviceReference.getBundle().getSymbolicName());
    switch (i) {
      case LogService.LOG_DEBUG:
        m_RootLogger.debug(m, s);
        break;
      case LogService.LOG_ERROR:
        m_RootLogger.error(m, s);
        break;
      case LogService.LOG_INFO:
        m_RootLogger.info(m, s);
        break;
      case LogService.LOG_WARNING:
        m_RootLogger.warn(m, s);
        break;
      default:
        m_RootLogger.error(m, s);
    }

  }//log

  public void log(ServiceReference serviceReference, int i, String s, Throwable throwable) {

    if (serviceReference == null) {
      log(i, s, throwable);
      return;
    }
    Marker m = MarkerFactory.getMarker(serviceReference.getBundle().getSymbolicName());
    switch (i) {
      case LogService.LOG_DEBUG:
        m_RootLogger.debug(m, s, throwable);
        break;
      case LogService.LOG_ERROR:
        m_RootLogger.error(m, s, throwable);
        break;
      case LogService.LOG_INFO:
        m_RootLogger.info(m, s, throwable);
        break;
      case LogService.LOG_WARNING:
        m_RootLogger.warn(m, s, throwable);
        break;
      default:
        m_RootLogger.error(m, s, throwable);
    }

  }//log

  public Logger getLogger(String str) {
    return Activator.getLoggerContext().getLogger(str);
  }//getLogger

  public Marker getMarker(String str) {
    return m_MarkerFactory.getMarker(str);
  }//getMarker

  public Logger getLogger(Class cl) {
    return getLogger(cl.getName());
  }//getLogger

  public Marker getMarker(Class cl) {
    return m_MarkerFactory.getMarker(cl.getName());
  }//getMarker

  private class FrameworkListenerAdapter
      implements FrameworkListener {

    private Logger m_Log = LoggingServiceImpl.this.getLogger("Framework");

    public FrameworkListenerAdapter() {
    }//constructor

    public void frameworkEvent(FrameworkEvent event) {


      final String bn = (event.getBundle() != null) ? event.getBundle().getSymbolicName() : "";
      Marker marker = MarkerFactory.getMarker(bn);

      switch (event.getType()) {
        case FrameworkEvent.ERROR:
          m_Log.error(marker, m_Resources.getString("framework.error"), event.getThrowable());
          break;
        case FrameworkEvent.INFO:
          m_Log.info(marker, m_Resources.getString("framework.info")); //? What is it good for?
          break;
        case FrameworkEvent.PACKAGES_REFRESHED:
          m_Log.info(marker, m_Resources.getString("framework.packages.refreshed"));
          break;
        case FrameworkEvent.STARTED:
          m_Log.info(marker, m_Resources.getString("framework.started"));
          break;
        case FrameworkEvent.STARTLEVEL_CHANGED:
          m_Log.info(marker, m_Resources.getString("framework.startlevel.change"), Activator.getServices().getStartLevel());
          break;
        case FrameworkEvent.WARNING:
          m_Log.info(marker, m_Resources.getString("framework.warning"));
          break;
        default:
          m_RootLogger.info(m_Resources.getString("error.framework.eventtype"), event.getType());
      }

    }//frameworkEvent

  }//FrameworkListenerAdapter

  private class BundleListenerAdapter
      implements BundleListener {

    private Logger m_Log = LoggingServiceImpl.this.getLogger("Bundles");

    public BundleListenerAdapter() {
    }//constructor

    public void bundleChanged(BundleEvent event) {
      Bundle b = event.getBundle();
      if (b == null) {
        m_RootLogger.error(m_Resources.getString("error.bundle.invalid"));
        return;
      }
      final String bn = (b.getSymbolicName() != null) ? b.getSymbolicName() : "[SN MISSING]";
      final Long bid = b.getBundleId(); //auto-boxed

      Marker marker = MarkerFactory.getMarker(bn);

      switch (event.getType()) {
        case BundleEvent.INSTALLED:
          m_Log.info(marker, m_Resources.getString("bundle.installed"), bid);
          break;
        case BundleEvent.RESOLVED:
          m_Log.info(marker, m_Resources.getString("bundle.resolved"), bid);
          break;
        case BundleEvent.STARTING:
          m_Log.info(marker, m_Resources.getString("bundle.starting"), bid);
          break;
        case BundleEvent.STARTED:
          m_Log.info(marker, m_Resources.getString("bundle.started"), bid);
          break;
        case BundleEvent.STOPPING:
          m_Log.info(marker, m_Resources.getString("bundle.stopping"), bid);
          break;
        case BundleEvent.STOPPED:
          m_Log.info(marker, m_Resources.getString("bundle.stopped"), bid);
          break;
        case BundleEvent.UPDATED:
          m_Log.info(marker, m_Resources.getString("bundle.updated"), bid);
          break;
        case BundleEvent.UNRESOLVED:
          m_Log.info(marker, m_Resources.getString("bundle.unresolved"), bid);
          break;
        case BundleEvent.UNINSTALLED:
          m_Log.info(marker, m_Resources.getString("bundle.uninstalled"), bid);
          break;
        default:
          m_RootLogger.info(marker, m_Resources.getString("error.bundle.eventtype"), bid, event.getType());
          break;
      }
    }//bundleChanged

  }//class BundleListenerAdapter


  private class ServiceListenerAdapter implements AllServiceListener {

    private Logger m_Log = LoggingServiceImpl.this.getLogger("Services");

    public void serviceChanged(ServiceEvent event) {

      ServiceReference sr = event.getServiceReference();
      Object o = sr.getProperty(org.osgi.framework.Constants.SERVICE_PID);
      String spid = (o != null) ? o.toString() : sr.toString();

      switch (event.getType()) {
        case ServiceEvent.REGISTERED:
          m_Log.info(m_Resources.getString("service.registered"), spid);
          break;
        case ServiceEvent.MODIFIED:
          m_Log.info(m_Resources.getString("service.modified"), spid);
          break;
        case ServiceEvent.UNREGISTERING:
          m_Log.info(m_Resources.getString("service.unregistering"), spid);
          break;
        default:
          m_RootLogger.info(m_Resources.getString("error.bundle.eventtype"), spid, event.getType());
      }

    }//serviceChanged

  }//ServiceListenerAdapter


}//class LoggingServiceIml
