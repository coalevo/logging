/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.logging.impl;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.event.SaxEvent;
import ch.qos.logback.core.joran.event.SaxEventRecorder;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.xml.sax.InputSource;

import javax.xml.parsers.SAXParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * This class implements the <tt>BundleActivator</tt> for this bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  public static Logger log;
  private Marker m_LogMarker;

  private static ServiceMediator c_Services;
  private static LoggingServiceImpl c_LoggingService;
  private static LoggerContext c_LoggerContext;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if(this.m_StartThread != null && m_StartThread.isAlive()) {
      throw new Exception();
    }

    c_LoggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    log = c_LoggerContext.getLogger(bundleContext.getBundle().getSymbolicName());

    //Start asynch
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //1. configure logging
              prepareLogback(System.getProperty("user.dir") + File.separator + "configuration" + File.separator + "logback.xml");

              c_LoggingService = new LoggingServiceImpl();
              if (!c_LoggingService.activate(bundleContext)) {
                log.error(m_LogMarker, "start(BundleContext)", "LoggingService.activate(BundleContext)");
                return;
              }
              log.info(m_LogMarker, "started");
            } catch (Exception ex) {
              log.error(m_LogMarker, "start()", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    if (c_Services != null) {
      c_Services.deactivate();
    }
    log.info(m_LogMarker, "stopped");

    log = null;
    m_LogMarker = null;
    c_LoggerContext = null;
    c_LoggingService = null;
    c_Services = null;
  }//stop

  public static void prepareLogback(String conffile) throws Exception {

    //1. Get file
    File f = new File(conffile);

    //2. Use a hacked SaxEventRecorder to obtain SAX properly
    List<SaxEvent> sev = null;
    try {
      SaxEventRecorder recorder = new MySaxEventRecorder();
      recorder.setContext(c_LoggerContext);
      sev = recorder.recordEvents(new InputSource(new FileInputStream(f)));
    } catch (JoranException je) {
      StatusPrinter.print(c_LoggerContext);
    }
    //4. go configure
    JoranConfigurator configurator = new JoranConfigurator();
    configurator.setContext(c_LoggerContext);
    c_LoggerContext.shutdownAndReset();
    configurator.doConfigure(sev);
  }//prepareLogBack


  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static LoggerContext getLoggerContext() {
    return c_LoggerContext;
  }//getLoggerContext

  private static class MySaxEventRecorder extends SaxEventRecorder {

    public List<SaxEvent> recordEvents(InputSource inputSource)
        throws JoranException {
      SAXParser saxParser = null;
      try {
        saxParser = c_Services.getSAXParserFactory(ServiceMediator.WAIT_UNLIMITED).newSAXParser();

      } catch (Exception pce) {
        String errMsg = "Parser configuration error occured";
        addError(errMsg, pce);
        throw new JoranException(errMsg, pce);
      }
      try {
        saxParser.parse(inputSource, this);
        return saxEventList;

      } catch (IOException ie) {
        String errMsg = "I/O error occurred while parsing xml file";
        addError(errMsg, ie);
        throw new JoranException(errMsg, ie);
      } catch (Exception ex) {
        String errMsg = "Problem parsing XML document. See previously reported errors. Abandoning all further processing.";
        addError(errMsg, ex);
        throw new JoranException(errMsg, ex);
      }

    }
  }//MySaxEventRecorder

}//class Activator
