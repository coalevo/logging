/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.logging.model;

import net.coalevo.logging.service.LoggingService;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.Marker;

/**
 * This class implements a mediator for a bundle log.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class LogProxy implements Logger {

  private BundleContext m_BundleContext;
  private LoggingService m_LoggingService;
  private Logger m_Logger;

  public String getName() {
    if (m_Logger != null) {
      return m_Logger.getName();
    } else {
      return "System";
    }
  }//getName

  //***** INFO *****//

  // without marker

  public boolean isInfoEnabled() {
    return m_Logger == null || m_Logger.isInfoEnabled();
  }//isInfoEnabled

  public void info(String msg) {
    if (m_Logger != null) {
      m_Logger.info(msg);
    } else {
      sysout(null, msg, null);
    }
  }//info

  public void info(String s, Object o) {
    if (m_Logger != null) {
      m_Logger.info(s, o);
    } else {
      sysout(null, String.format(s, o), null);
    }
  }//info

  public void info(String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.info(s, o, o1);
    } else {
      sysout(null, String.format(s, o, o1), null);
    }
  }//info

  public void info(String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.info(s, objects);
    } else {
      sysout(null, String.format(s, objects), null);
    }
  }//info

  public void info(String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.info(s, throwable);
    } else {
      sysout(null, s, throwable);
    }
  }//info

  // with marker
  public boolean isInfoEnabled(Marker marker) {
    return m_Logger == null || m_Logger.isInfoEnabled(marker);
  }//isInfoEnabled


  public void info(Marker marker, String msg) {
    if (m_Logger != null) {
      m_Logger.info(marker, msg);
    } else {
      sysout(marker.getName(), msg, null);
    }
  }//info

  public void info(Marker marker, String s, Object o) {
    if (m_Logger != null) {
      m_Logger.info(s, o);
    } else {
      sysout(marker.toString(), String.format(s, o), null);
    }
  }//info

  public void info(Marker marker, String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.info(s, o, o1);
    } else {
      sysout(marker.toString(), String.format(s, o, o1), null);
    }
  }//info

  public void info(Marker marker, String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.info(s, objects);
    } else {
      sysout(marker.toString(), String.format(s, objects), null);
    }
  }//info

  public void info(Marker marker, String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.info(s, throwable);
    } else {
      sysout(marker.toString(), s + "::" + throwable.toString(), null);
    }
  }//info

  //****** DEBUG ******//

  public boolean isDebugEnabled() {
    return m_Logger == null || m_Logger.isInfoEnabled();
  }//isInfoEnabled

  public void debug(String msg) {
    if (m_Logger != null) {
      m_Logger.debug(msg);
    } else {
      sysout(null, msg, null);
    }
  }//debug

  public void debug(String s, Object o) {
    if (m_Logger != null) {
      m_Logger.debug(s, o);
    } else {
      sysout(null, String.format(s, o), null);
    }
  }//debug

  public void debug(String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.debug(s, o, o1);
    } else {
      sysout(null, String.format(s, o, o1), null);
    }
  }//debug

  public void debug(String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.debug(s, objects);
    } else {
      sysout(null, String.format(s, objects), null);
    }
  }//debug

  public void debug(String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.debug(s, throwable);
    } else {
      sysout(null, s, throwable);
    }
  }//debug

  // with marker
  public boolean isDebugEnabled(Marker marker) {
    return m_Logger == null || m_Logger.isInfoEnabled(marker);
  }//isInfoEnabled


  public void debug(Marker marker, String msg) {
    if (m_Logger != null) {
      m_Logger.debug(marker, msg);
    } else {
      sysout(marker.getName(), msg, null);
    }
  }//debug

  public void debug(Marker marker, String s, Object o) {
    if (m_Logger != null) {
      m_Logger.debug(s, o);
    } else {
      sysout(marker.toString(), String.format(s, o), null);
    }
  }//debug

  public void debug(Marker marker, String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.debug(s, o, o1);
    } else {
      sysout(marker.toString(), String.format(s, o, o1), null);
    }
  }//debug

  public void debug(Marker marker, String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.debug(s, objects);
    } else {
      sysout(marker.toString(), String.format(s, objects), null);
    }
  }//debug

  public void debug(Marker marker, String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.debug(s, throwable);
    } else {
      sysout(marker.toString(), s, throwable);
    }
  }//debug

  //****** TRACE ******//

  public boolean isTraceEnabled() {
    return m_Logger == null || m_Logger.isInfoEnabled();
  }//isInfoEnabled

  public void trace(String msg) {
    if (m_Logger != null) {
      m_Logger.trace(msg);
    } else {
      sysout(null, msg, null);
    }
  }//trace

  public void trace(String s, Object o) {
    if (m_Logger != null) {
      m_Logger.trace(s, o);
    } else {
      sysout(null, String.format(s, o), null);
    }
  }//trace

  public void trace(String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.trace(s, o, o1);
    } else {
      sysout(null, String.format(s, o, o1), null);
    }
  }//trace

  public void trace(String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.trace(s, objects);
    } else {
      sysout(null, String.format(s, objects), null);
    }
  }//trace

  public void trace(String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.trace(s, throwable);
    } else {
      sysout(null, s, throwable);
    }
  }//trace

  // with marker
  public boolean isTraceEnabled(Marker marker) {
    return m_Logger == null || m_Logger.isInfoEnabled(marker);
  }//isInfoEnabled


  public void trace(Marker marker, String msg) {
    if (m_Logger != null) {
      m_Logger.trace(marker, msg);
    } else {
      sysout(marker.getName(), msg, null);
    }
  }//trace

  public void trace(Marker marker, String s, Object o) {
    if (m_Logger != null) {
      m_Logger.trace(s, o);
    } else {
      sysout(marker.toString(), String.format(s, o), null);
    }
  }//trace

  public void trace(Marker marker, String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.trace(s, o, o1);
    } else {
      sysout(marker.toString(), String.format(s, o, o1), null);
    }
  }//trace

  public void trace(Marker marker, String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.trace(s, objects);
    } else {
      sysout(marker.toString(), String.format(s, objects), null);
    }
  }//trace

  public void trace(Marker marker, String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.trace(s, throwable);
    } else {
      sysout(marker.toString(), s, throwable);
    }
  }//trace

  //****** ERROR ******//

  // without marker

  public boolean isErrorEnabled() {
    return m_Logger == null || m_Logger.isErrorEnabled();
  }//isErrorEnabled

  public void error(String msg) {
    if (m_Logger != null) {
      m_Logger.error(msg);
    } else {
      syserr(null, msg, null);
    }
  }//error

  public void error(String s, Object o) {
    if (m_Logger != null) {
      m_Logger.error(s, o);
    } else {
      syserr(null, String.format(s, o), null);
    }
  }//error

  public void error(String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.error(s, o, o1);
    } else {
      syserr(null, String.format(s, o, o1), null);
    }
  }//error

  public void error(String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.error(s, objects);
    } else {
      syserr(null, String.format(s, objects), null);
    }
  }//error

  public void error(String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.error(s, throwable);
    } else {
      syserr(null, s, throwable);
    }
  }//error

  // with marker
  public boolean isErrorEnabled(Marker marker) {
    return m_Logger == null || m_Logger.isErrorEnabled(marker);
  }//isErrorEnabled


  public void error(Marker marker, String msg) {
    if (m_Logger != null) {
      m_Logger.error(marker, msg);
    } else {
      syserr(marker.getName(), msg, null);
    }
  }//error

  public void error(Marker marker, String s, Object o) {
    if (m_Logger != null) {
      m_Logger.error(s, o);
    } else {
      syserr(marker.toString(), String.format(s, o), null);
    }
  }//error

  public void error(Marker marker, String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.error(s, o, o1);
    } else {
      syserr(marker.toString(), String.format(s, o, o1), null);
    }
  }//error

  public void error(Marker marker, String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.error(s, objects);
    } else {
      syserr(marker.toString(), String.format(s, objects), null);
    }
  }//error

  public void error(Marker marker, String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.error(s, throwable);
    } else {
      syserr(marker.toString(), s + "::" + throwable.toString(), null);
    }
  }//error

  //***** WARN ******//
  // without marker

  public boolean isWarnEnabled() {
    return m_Logger == null || m_Logger.isWarnEnabled();
  }//isWarnEnabled

  public void warn(String msg) {
    if (m_Logger != null) {
      m_Logger.warn(msg);
    } else {
      syserr(null, msg, null);
    }
  }//warn

  public void warn(String s, Object o) {
    if (m_Logger != null) {
      m_Logger.warn(s, o);
    } else {
      syserr(null, String.format(s, o), null);
    }
  }//warn

  public void warn(String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.warn(s, o, o1);
    } else {
      syserr(null, String.format(s, o, o1), null);
    }
  }//warn

  public void warn(String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.warn(s, objects);
    } else {
      syserr(null, String.format(s, objects), null);
    }
  }//warn

  public void warn(String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.warn(s, throwable);
    } else {
      syserr(null, s, throwable);
    }
  }//warn

  // with marker
  public boolean isWarnEnabled(Marker marker) {
    return m_Logger == null || m_Logger.isWarnEnabled(marker);
  }//isWarnEnabled


  public void warn(Marker marker, String msg) {
    if (m_Logger != null) {
      m_Logger.warn(marker, msg);
    } else {
      syserr(marker.getName(), msg, null);
    }
  }//warn

  public void warn(Marker marker, String s, Object o) {
    if (m_Logger != null) {
      m_Logger.warn(s, o);
    } else {
      syserr(marker.toString(), String.format(s, o), null);
    }
  }//warn

  public void warn(Marker marker, String s, Object o, Object o1) {
    if (m_Logger != null) {
      m_Logger.warn(s, o, o1);
    } else {
      syserr(marker.toString(), String.format(s, o, o1), null);
    }
  }//warn

  public void warn(Marker marker, String s, Object[] objects) {
    if (m_Logger != null) {
      m_Logger.warn(s, objects);
    } else {
      syserr(marker.toString(), String.format(s, objects), null);
    }
  }//warn

  public void warn(Marker marker, String s, Throwable throwable) {
    if (m_Logger != null) {
      m_Logger.warn(s, throwable);
    } else {
      syserr(marker.toString(), s + "::" + throwable.toString(), null);
    }
  }//warn


  private void sysout(String marker, String msg, Throwable t) {
    //Assemble String
    StringBuilder sbuf = new StringBuilder();
    Bundle b = m_BundleContext.getBundle();
    sbuf.append(b.getHeaders().get(Constants.BUNDLE_NAME));
    sbuf.append(" [");
    sbuf.append(b.getBundleId());
    if (marker != null) {
      sbuf.append("::");
      sbuf.append(marker);
    }
    sbuf.append("] ");
    sbuf.append(msg);
    System.out.println(sbuf.toString());
    if (t != null) {
      t.printStackTrace(System.err);
    }
  }//sysout

  private void syserr(String marker, String msg, Throwable t) {
    //Assemble String
    StringBuffer sbuf = new StringBuffer();
    Bundle b = m_BundleContext.getBundle();
    sbuf.append(b.getHeaders().get(Constants.BUNDLE_NAME));
    sbuf.append(" [");
    sbuf.append(b.getBundleId());
    if (marker != null) {
      sbuf.append("::");
      sbuf.append(marker);
    }
    sbuf.append("] ");
    sbuf.append(msg);
    System.err.println(sbuf.toString());
    if (t != null) {
      t.printStackTrace(System.err);
    }
  }//logToSystem

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + LoggingService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    m_LoggingService = null;
    m_Logger = null;
    m_BundleContext = null;
  }//deactivate

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof LoggingService) {
            m_LoggingService = (LoggingService) o;
            //prepare Log
            m_Logger = m_LoggingService.getLogger(m_BundleContext.getBundle().getSymbolicName());
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof LoggingService) {
            m_LoggingService = null;
            m_Logger = null;
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class LogProxy
