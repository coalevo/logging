/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.logging.service;

import org.osgi.service.log.LogService;
import org.slf4j.Logger;
import org.slf4j.Marker;

/**
 * Defines an interface for the logging service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface LoggingService
    extends LogService {

  /**
   * Returns a logger (org.slf4j) with the given hierachical name.
   *
   * @param str the name of the logger (hierarchy with .)
   * @return the <tt>Logger</tt>.
   */
  public Logger getLogger(String str);

  /**
   * Returns a marker (org.slf4j) with the given hierarchical name.
   *
   * @param str the name of the marker (hierarchy with .)
   * @return the <tt>Marker</tt>.
   */
  public Marker getMarker(String str);

  /**
   * Returns a logger (org.slf4j) with the given hierachical name.
   * <p>
   * This is essentially a convenience method that will call {@link #getLogger(String)}
   * with <tt>cl.getName()</tt>.
   * </p>
   *
   * @param cl a class to obtain the named hierarchy from the fully qualified class name.
   * @return the <tt>Logger</tt>.
   */
  public Logger getLogger(Class cl);

   /**
   * Returns a marker (org.slf4j) with the given hierachical name.
   * <p>
   * This is essentially a convenience method that will call {@link #getMarker(String)}
   * with <tt>cl.getName()</tt>.
   * </p>
   *
   * @param cl a class to obtain the named hierarchy from the fully qualified class name.
   * @return the <tt>Logger</tt>.
   */
  public Marker getMarker(Class cl);

}//interface LoggingService
