/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.logging.filter;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * This class implements a filter that will match logger names.
 * <p/>
 * Kind of a standard filtering I always used in log4j.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class LoggerNameFilter extends AbstractMatcherFilter {

  private String m_LoggerName;

  public FilterReply decide(Object o) {
    LoggingEvent event = (LoggingEvent) o;

    if (event.getLoggerRemoteView().getName().startsWith(m_LoggerName)) {
      return onMatch;
    } else {
      return onMismatch;
    }
  }//decide

  public void setLoggerName(String loggerName) {
    m_LoggerName = loggerName;
  }//setLoggerName

  public String getLoggerName() {
    return m_LoggerName;
  }//getLoggerName

  public void start() {
    if (m_LoggerName != null) {
      super.start();
    }
  }//start

}//class LoggerNameFilter
